package com.example.raulpedraza.android1fundamentos.Utils;

import android.app.Application;
import android.util.Log;

import com.example.raulpedraza.android1fundamentos.Models.Table;
import com.example.raulpedraza.android1fundamentos.Utils.TableAndMenuRepository;

/**
 * Created by raulpedraza on 28/4/16.
 */
public class RApplication extends Application {

    private final TableAndMenuRepository tableAndMenuRepository = new TableAndMenuRepository();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public TableAndMenuRepository getTableAndMenuRepository() {
        return tableAndMenuRepository;
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    Log.d(this.getClass().getName(),"pico!!!");
    }
}
