package com.example.raulpedraza.android1fundamentos.Models;

import java.util.List;

/**
 * Created by raulpedraza on 15/4/16.
 */
public class Table {

    String aNameTable;
    private List<Menu> menuList;

    public Table(String aNumberTable, List<Menu> menuList) {
        this.aNameTable = aNumberTable;
        this.menuList = menuList;
    }

    public String getNambeTable() {
        return aNameTable;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setaNambeTable(String aNambeTable) {
        this.aNameTable = aNambeTable;
    }
}
