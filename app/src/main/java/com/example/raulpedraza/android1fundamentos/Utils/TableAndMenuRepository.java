package com.example.raulpedraza.android1fundamentos.Utils;

import android.support.annotation.NonNull;

import com.example.raulpedraza.android1fundamentos.Models.Menu;
import com.example.raulpedraza.android1fundamentos.Models.Table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by raulpedraza on 19/4/16.
 */
public class TableAndMenuRepository {
    private final ArrayList<Table> tables = new ArrayList<>();
    private final ArrayList<Menu> menus = new ArrayList<>();


    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }

    public TableAndMenuRepository() {
        tables.add(new Table("Mesa 1", createMenus()));
        tables.add(new Table("Mesa 2", createMenus()));
        tables.add(new Table("Mesa 3", createMenus()));
        tables.add(new Table("Mesa 4", createMenus()));
        tables.add(new Table("Mesa 5", createMenus()));
        tables.add(new Table("Mesa 6", createMenus()));
        tables.add(new Table("Mesa 7", createMenus()));
        tables.add(new Table("Mesa 8", createMenus()));
    }

    @NonNull
    private ArrayList<Menu> createMenus() {
        menus.add(new Menu("Menu 1","http://www.hola.com/imagenes/cocina/noticiaslibros/2014100774124/millesime-madrid-2014/0-290-520/plato_millesime_-z.jpg",false));
        menus.add(new Menu("Menu 2","http://www.kienyke.com/wp-content/uploads/2013/08/Fish-Market-2.jpg",false));
        menus.add(new Menu("Menu 3","http://www.simplyfosh.com/img/gallery_thumbs/17.jpg",false));
        menus.add(new Menu("Menu 4","https://www.larepublica.net/app/cms/www/images/201403062319260.m10.jpg",false));
        menus.add(new Menu("Menu 5","http://cadenaser00.epimg.net/ser/imagenes/2013/01/10/gastro/1357780995_740215_0000000000_noticia_normal.jpg",false));
        menus.add(new Menu("Menu 6","http://www.hola.com/imagenes/cocina/noticiaslibros/2014100774124/millesime-madrid-2014/0-290-520/plato_millesime_-z.jpg",false));
        menus.add(new Menu("Menu 7","http://www.kienyke.com/wp-content/uploads/2013/08/Fish-Market-2.jpg",false));
        menus.add(new Menu("Menu 8","http://www.simplyfosh.com/img/gallery_thumbs/17.jpg",false));
        menus.add(new Menu("Menu 9","https://www.larepublica.net/app/cms/www/images/201403062319260.m10.jpg",false));
        menus.add(new Menu("Menu 10","http://cadenaser00.epimg.net/ser/imagenes/2013/01/10/gastro/1357780995_740215_0000000000_noticia_normal.jpg",false));

        return menus;
    }

    public ArrayList<Table> getTables() {
        return tables;
    }

    public Table getTable(String tableId) {
        for (Table table : tables) {
            if(table.getNambeTable().equals(tableId)) {
                return table;
            }
        }
        return null;
    }
}
