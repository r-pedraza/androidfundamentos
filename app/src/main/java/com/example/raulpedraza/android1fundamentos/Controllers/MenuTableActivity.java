package com.example.raulpedraza.android1fundamentos.Controllers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.raulpedraza.android1fundamentos.Controllers.Adapters.MenuAdapter;
import com.example.raulpedraza.android1fundamentos.Models.Table;
import com.example.raulpedraza.android1fundamentos.R;
import com.example.raulpedraza.android1fundamentos.Utils.RApplication;
import com.example.raulpedraza.android1fundamentos.Utils.TableAndMenuRepository;

/**
 * Created by raulpedraza on 16/4/16.
 */
public class MenuTableActivity extends AppCompatActivity {

    FloatingActionButton saveOrderButton;
    String tableId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_table);
        tableId = getIntent().getExtras().getString("tableId");

        RApplication application = (RApplication) getApplication();
        TableAndMenuRepository tableAndMenuRepository = application.getTableAndMenuRepository();

        Table table = tableAndMenuRepository.getTable(tableId);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.menu_recycler_view);
        FloatingActionButton fab1 = (FloatingActionButton)findViewById(R.id.menu_fab_save);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext(),"Comanda guardada",Toast.LENGTH_LONG).show();
                ((MenuTableActivity) v.getContext()).onBackPressed();
            }
        });

        GridLayoutManager linearLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(linearLayoutManager);

        MenuAdapter adapter = new MenuAdapter(table.getMenuList());
        recyclerView.setAdapter(adapter);
    }
}
