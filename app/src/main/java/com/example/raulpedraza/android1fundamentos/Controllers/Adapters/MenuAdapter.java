package com.example.raulpedraza.android1fundamentos.Controllers.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.raulpedraza.android1fundamentos.Models.Menu;
import com.example.raulpedraza.android1fundamentos.R;

import java.util.List;

import com.example.raulpedraza.android1fundamentos.Utils.Core;

/**
 * Created by raulpedraza on 15/4/16.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    public  List<Menu>menuList;
    private View.OnClickListener listener;

    public MenuAdapter(List<Menu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public void onBindViewHolder(final MenuAdapter.MenuViewHolder holder, final int position) {
        final Menu menu = menuList.get(position);

        holder.menuTitle.setText(menuList.get(position).getName());
        Core core = new Core(holder.menuImage);
        core.execute(menuList.get(position).getImageURL());
        holder.menuImage = core.bmImage;
        holder.menuCheck.setChecked(menu.getSelected());
        holder.menuCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                menu.setSelected(isChecked);
            }
        });

    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_card,parent,false);
        MenuViewHolder menuViewHolder = new MenuAdapter.MenuViewHolder(view);

        return menuViewHolder;
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }


    public static class MenuViewHolder extends RecyclerView.ViewHolder{

        TextView    menuTitle;
        CheckBox    menuCheck;
        ImageView   menuImage;


        public MenuViewHolder(View itemView) {
            super(itemView);
            menuTitle       = (TextView)itemView.findViewById(R.id.menu_title);
            menuCheck       = (CheckBox)itemView.findViewById(R.id.menu_check);
            menuImage       = (ImageView) itemView.findViewById(R.id.menu_image);
        }
    }

}
