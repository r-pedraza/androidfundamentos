package com.example.raulpedraza.android1fundamentos.Controllers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.raulpedraza.android1fundamentos.Controllers.Adapters.TableAdapter;
import com.example.raulpedraza.android1fundamentos.Models.Table;
import com.example.raulpedraza.android1fundamentos.R;
import com.example.raulpedraza.android1fundamentos.Utils.RApplication;
import com.example.raulpedraza.android1fundamentos.Utils.TableAndMenuRepository;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RApplication application = (RApplication) getApplication();
        TableAndMenuRepository tableAndMenuRepository = application.getTableAndMenuRepository();

        ArrayList<Table> tables = tableAndMenuRepository.getTables();

        final RecyclerView recyclerView = (RecyclerView)findViewById(R.id.table_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        TableAdapter adapter = new TableAdapter(tables);
        recyclerView.setAdapter(adapter);
    }
}
