package com.example.raulpedraza.android1fundamentos.Models;

import android.media.Image;

/**
 * Created by raulpedraza on 16/4/16.
 */
public class Menu {
    String  name;
    String  imageURL;
    Boolean isSelected;

    public Menu(String name, String imageURL,Boolean isSelected) {
        this.name       = name;
        this.imageURL   = imageURL;
        this.isSelected = isSelected;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

}
