package com.example.raulpedraza.android1fundamentos.Controllers.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.raulpedraza.android1fundamentos.Controllers.MenuTableActivity;
import com.example.raulpedraza.android1fundamentos.Models.Menu;
import com.example.raulpedraza.android1fundamentos.Models.Table;
import com.example.raulpedraza.android1fundamentos.R;

import java.util.List;

/**
 * Created by raulpedraza on 15/4/16.
 */
public class TableAdapter extends RecyclerView.Adapter<TableAdapter.TableViewHolder> implements View.OnClickListener {


    public  List<Table>tableList;
    private View.OnClickListener listener;

    public TableAdapter(List<Table> tableList) {
        this.tableList = tableList;
    }

    @Override
    public TableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_card,parent,false);
        TableViewHolder tableViewHolder = new TableViewHolder(view);
        view.setOnClickListener((View.OnClickListener) this);

        return tableViewHolder;
    }

    @Override
    public void onBindViewHolder(final TableViewHolder holder, final int position) {

        holder.tableTitle.setText(tableList.get(position).getNambeTable());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), MenuTableActivity.class);
                intent.putExtra("tableId", holder.tableTitle.getText());
                v.getContext().startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return tableList.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);

    }

    public static class TableViewHolder extends RecyclerView.ViewHolder{

        TextView tableTitle;
        private final Context context;

        public TableViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            tableTitle = (TextView)itemView.findViewById(R.id.table_title);
        }
    }

}
